using System;

namespace AnimalAsksForFood {
    public class Man {
        public string ManGivesFood (bool isHorseIsHungry) {
            if (isHorseIsHungry) {

                return "Eat it!";
            } else {
                return "Nothing";
            }
        }

    }
}