using System;
using NUnit.Framework;       
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestApiTesting.Tests
{
    [TestFixture]
    ///<summary>this class for testing API collections(GET, POST, PUT, DELETE)</summary>
    public class RestApiTest
    {
        ///<summary>the base-URL, 'endpointURL' is defined</summary>
        private string endpointURL = "https://reqres.in";

        ///<summary>HttpClient Class</summary>
        private HttpClient httpClient;

        ///<summary>data to be passed to POST method      
        private static List<KeyValuePair<string, string>> postData = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string> ("name", "morpheus"),
            new KeyValuePair<string, string> ("job", "leader")
        };

        ///<summary>data to be passed to PUT method
        private static List<KeyValuePair<string, string>> putData = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string> ("name", "morpheus"),
            new KeyValuePair<string, string> ("job", "zion resident")
        };

        ///<summary>This attribute is used inside a TestFixture to provide a common set of functions that are performed just before each test method is called </summary>
        [SetUp]
        public void Init()
        {
            httpClient = new HttpClient();
        }

        ///<summary>This test method to check whether expected resource/data is fetched from server, when valid URL is passed along with expected StatusCode in GET request </summary> 
        ///<param> passing parameterURL and StatusCode=200(OK) and 404(Not Found) to GetMethod</param>
        [TestCase("123",404)]
        [TestCase("3",404)]
        [TestCase("2", 404)]
        [TestCase("",404)]
        [TestCase("1",200)]
        [Test]
        public void GetMethod_for_ValidorInvalidURL(string parameterURL, int StatusCode)
        {
            string getURL = endpointURL + "/api/users/"+ parameterURL;
            Uri getUri = new Uri(getURL);
            Task<HttpResponseMessage> httpResponse = httpClient.GetAsync(getUri);
            Assert.That(StatusCode,Is.EqualTo((int)httpResponse.Result.StatusCode), "Invalid URl");
        }

        ///<summary>this test method to create the resource on the server, is true when statuscode= 201</summary>
        [Test]
        public void PostMethod_StatusCodeTest()
        {
            string parameterURL = "/api/users";
            string postURL = endpointURL + parameterURL;
            Uri postUri = new Uri(postURL);
            List<KeyValuePair<string, string>> data= postData;
            HttpContent content= new FormUrlEncodedContent(data);
            Task<HttpResponseMessage> httpResponse = httpClient.PostAsync(postUri, content);
            Assert.That(201, Is.EqualTo((int)httpResponse.Result.StatusCode),"Resource on the server is not created!");
        }
        
        ///<summary>this test method to update the existing resource on the server, is true when statuscode= 200</summary>
        [Test]
        public void PutMethod_StatusCodeTest()
        {
            string parameterURL = "/api/users/2";
            string putURL = endpointURL + parameterURL;
            Uri putUri = new Uri(putURL);
            List<KeyValuePair<string, string>> data= putData;
            HttpContent content = new FormUrlEncodedContent(data);
            Task<HttpResponseMessage> httpResponse = httpClient.PutAsync(putUri, content);
            Assert.That(200, Is.EqualTo((int)httpResponse.Result.StatusCode), "Resource is not updated on the server!");
        }

        ///<summary>this test method to delete the resource on the server, is true when statuscode= 204(No Content)</summary>
        ///<param>expected status=204 as a parameter</param>
        [TestCase(204)]
        [Test]
        public void DeleteMethod_StatusCodeTest(int StatusCode)
        {
            string parameterURL = "/api/users/2";
            string deleteURL = endpointURL + parameterURL;
            Task<HttpResponseMessage> httpResponse = httpClient.DeleteAsync(deleteURL);
            Assert.That(StatusCode, Is.EqualTo((int)httpResponse.Result.StatusCode),"Resource not deleted on the server!");
        }

        ///<summary>This attribute is used inside a TestFixture to provide a common set of functions that are performed after each test method is run</summary>
        [TearDown]
        public void Dispose()
        {
            httpClient.Dispose();
        }
    }
}