#!/usr/bin/python3
def sameSystemEventsWithoutTimestamp(e1, e2):

    sysEvent1 = e1.split("\n")
    sysEvent2 = e2.split("\n")

    assert len(sysEvent1) == len(sysEvent2), (
        "[TEST SCRIPT] two given strings are different, expected line numbers "
        "is {} but {}".format(len(sysEvent1), len(sysEvent2)))

    for i in range(0, len(sysEvent1)):
        tmp = sysEvent1[i].split(",")
        tmp1 = [tmp[0]] + tmp[2:len(tmp)]
        tmp1 = ",".join(tmp1)

        tmp = sysEvent2[i].split(",")
        tmp2 = [tmp[0]] + tmp[2:len(tmp)]
        tmp2 = ",".join(tmp2)

        assert tmp1 == tmp2, (
            "\n[TEST SCRIPT] two given strings has different lines at {}:'{}' "
            "expected but '{}'".format(i, tmp1, tmp2))


"""
$$$$$_BEGIN_TEST_METADATA_DECLARATION_$$$$$
    +purpose: SystemEvents are saved to SystemEventLogger and retrieved from it via Rpc
    +test_tier: 2
    +is_manual: False
    +test_classification: System
    +reqid: DUNE-7316
    +timeout:120
    +asset: CoreFramework
    +test_framework: TUF
    +name: test_log_and_retrieve_system_events
    +test:
        +title: SystemEvent logging and retrieving via Rpc
        +guid:c414aff8-d9ce-4ef5-a30d-dc66803ba3e3
        +dut:
            +type:Simulator
$$$$$_END_TEST_METADATA_DECLARATION_$$$$$
"""
def test_log_and_retrieve_system_events_from_applications(udw):

    # INFO, WARNING, ERROR, CRITICAL
    eventType = [int("0x0000000000000000", 16), int("0x0001000000000000", 16),
                 int("0x0002000000000000", 16), int("0x0003000000000000", 16)]

    tInternalCode = [eventType[0] | 1, eventType[1] | 2, eventType[2] | 3]
    tEventData = ["testkey1:testdata1,conkey:condata", "testkey2:testdata2",
                  "testkey3:testdata3"]
    tFwVersion = ["testVerConnectivity", "testVerMainApp", "testVerMainUiApp"]
    tProcessName = ["connectivity", "mainApp", "mainAppTest"]

    udw.mainApp.SystemEventLogger.clear()

    udw.connectivityApp.SystemEventLogger.log(tInternalCode[0],
                                              tEventData[0],
                                              tFwVersion[0],
                                              tProcessName[0])

    udw.mainApp.SystemEventLogger.log(tInternalCode[1], tEventData[1],
                                      tFwVersion[1], tProcessName[1])
    udw.mainApp.SystemEventLogger.log(tInternalCode[2], tEventData[2],
                                      tFwVersion[2], tProcessName[2])

    # mainApp finds all
    # NOTE: braces like [ or ] are not supported. There is an issue between
    #       test framework and Udw command
    filter = ("code:1:{},fwVersion:test\\\\w+,processName:connectivity|"
              "mainApp.*,data:test\\\\w+:test\\\\w+".
              format(int("0x0F00000000000000", 16)))
    expected = ("INFO-00.00.00.01-00.00,   TIMESTAMP,testVerConnectivity,"
                "connectivity,2:conkey,Fcondata,testkey1,Ftestdata1\n"
                "WARNING-00.00.00.02-00.00,TIMESTAMP,testVerMainApp,mainApp,"
                "1:testkey2,Ftestdata2\n"
                "ERROR-00.00.00.03-00.00,  TIMESTAMP,testVerMainUiApp,"
                "mainAppTest,1:testkey3,Ftestdata3")

    result = udw.mainApp.SystemEventLogger.getSystemEvents(filter)
    sameSystemEventsWithoutTimestamp(expected, result)

    # mainApp finds one made by connectivity
    filter = ("code:1:2,fwVersion:testVerConnectivity,processName:c.+,"
              "data:con\\\\w+:con\\\\w+")
    expected = ("INFO-00.00.00.01-00.00,TIMESTAMP,testVerConnectivity,"
                "connectivity,2:conkey,Fcondata,testkey1,Ftestdata1")
    result = udw.mainApp.SystemEventLogger.getSystemEvents(filter)
    sameSystemEventsWithoutTimestamp(expected, result)

    # connectivity app finds one made by mainApp
    codeCondition = int("0x0001000000000009", 16)
    filter = ("code:0:{},fwVersion:testVerMain\\\\w+,processName:m.+,"
              "data:testkey.?:testdata.?".format(codeCondition))
    expected = ("WARNING-00.00.00.02-00.00,TIMESTAMP,testVerMainApp,mainApp,"
                "1:testkey2,Ftestdata2")
    result = udw.connectivityApp.SystemEventLogger.getSystemEvents(filter)
    sameSystemEventsWithoutTimestamp(expected, result)

    # connectivity app finds two made by mainApp
    codeCondition = int("0x0002000000000009", 16)
    filter = ("code:0:{},fwVersion:testVerMain\\\\w+,processName:m.+,"
              "data:testkey.?:testdata.?".format(codeCondition))
    expected = ("WARNING-00.00.00.02-00.00,TIMESTAMP,testVerMainApp,mainApp,"
                "1:testkey2,Ftestdata2\n"
                "ERROR-00.00.00.03-00.00,  TIMESTAMP,testVerMainUiApp,"
                "mainAppTest,1:testkey3,Ftestdata3")
    result = udw.connectivityApp.SystemEventLogger.getSystemEvents(filter)
    sameSystemEventsWithoutTimestamp(expected, result)
