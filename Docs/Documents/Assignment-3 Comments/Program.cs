﻿using System;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BlogDetails
{
    class DisplayBlogDetails
    {
        static int minimumRange, maximumRange;

        public static void ValidatingRange()
        {
            Console.WriteLine("Enter the minimum range:");
             minimumRange = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the maximum range");
            maximumRange = int.Parse(Console.ReadLine());
            if (maximumRange - minimumRange > 50)
            {
                Console.WriteLine("Maximum range of photos is 50, please try again!!");
                ValidatingRange();
            }
        }

        public static void PrintDataFromBlog(string response)
        {
            while (response.StartsWith("var tumblr_api_read = "))
            {
                response = response.Substring("var tumblr_api_read = ".Length);    //Removing the unwanted characters to make it a valid json
            }
            response = response.Replace(";", "");
            dynamic dynamicObject = JsonConvert.DeserializeObject<dynamic>(response);
            try
            {
                Console.WriteLine("title :" + dynamicObject["tumblelog"]["title"].ToString());
                Console.WriteLine("name :" + dynamicObject["tumblelog"]["name"].ToString());
                Console.WriteLine("description :" + dynamicObject["tumblelog"]["description"].ToString());
                Console.WriteLine("number of posts :" + dynamicObject["posts-total"].ToString());
                
                foreach (dynamic i in dynamicObject.posts)
                {
                    foreach (dynamic j in i)
                    {
                        if (("" + j).StartsWith("\"photo-url"))
                        {
                            foreach (dynamic k in j)
                            {
                                Console.WriteLine(k);
                            }
                            break;
                        }
                    }
                }
                Console.ReadLine();
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void Main(string[] args)
        {
            WebClient webClient = new WebClient();
            Console.WriteLine("Enter the Tumblr blog name:");
            string blogName = Console.ReadLine();
            ValidatingRange();
            string url = "https://" + blogName + ".tumblr.com/api/read/json?type=photo&num=" + maximumRange + "&start=" + minimumRange;
            PrintDataFromBlog(webClient.DownloadString(url));
        }   
    }
}

/*
 FORMATTING RULES
    - The name should be simple but self-explanatory.
    - The topmost parts of the source file should provide the high-level concepts.
    - Detail should increase as we move downward, until at the end we find the lowest level functions and details in the source file.
    - Vertical openness : Each line represents an expression or a clause, and each group of lines represents a complete thought. Those thoughts should be separated from each other with blank lines.
    - Vertical density : Lines of code that are tightly related should appear vertically dense.
    - Vertical distance : Concepts that are closely related should be kept vertically close to each other. Closely related concepts should not be separated into different files unless you have a very good reason.
    - Vertical declaration : Variables should be declared as close to their usage as possible.
    - Dependant functions : If one function calls another, they should be vertically close, the caller should be above the callee, if at all possible.
    - Conceptual affinity : Certain bits of code want to be near other bits. They have a certain conceptual affinity. The stronger that affinity, the less vertical distance there should be between them.
    - Horizontal Openness and Density : Surrounded the assignment operators with white space to separate them, no spaces between the function names and the opening parenthesis, separate arguments within the function call parenthesis to accentuate the comma and show that the arguments are separate.
    - Indentation : Statements at the level of the file, such as most class declarations, are not indented at all. Methods within a class are indented one level to the right of the class. Implementations of those methods are implemented one level to the right of the method declaration. Block implementations are implemented one level to the right of their containing block, and so on.
    - Avoid dummy scopes.
    - If in a project, make a set of common formatting rules and follow them.
 */
