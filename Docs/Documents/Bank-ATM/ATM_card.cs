namespace ATM_machine {

    public class ATMCard {
        private double balance = 1220;
        private double balanceInATM = 20000;
        private int pin = 1;
        public int getPin () {
            return pin;
        }

        public double withdraw (double amount) {
            if (balance < amount) {
                throw new InsufficientBalanceException ("Account does not have enough balance");
            }
            balance = balance - amount;
            return amount;
        }

        public double balanceEnquiry () {
            return balance;
        }
    }
}