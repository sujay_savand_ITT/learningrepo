using System;
using System.Net.Sockets;
namespace ATM_machine {
    [Serializable]
    class NetworkException : Exception {
        public NetworkException (string host, int port) 
        {
            Socket s = new Socket(AddressFamily.InterNetwork,SocketType.Stream,ProtocolType.Tcp);

            Console.WriteLine("Establishing Connection to {0}",
                host);
            try
            {
                s.Connect(host, port);
                Console.WriteLine("Connection established");
            }
            catch (SocketException e)
            {
                Console.WriteLine("Server not able to connect!!");
            }
        }
    }

    [Serializable]
    class InsufficientBalanceException : Exception {
        public InsufficientBalanceException () { }

        public InsufficientBalanceException (string message) : base (String.Format (message)) { }

    }
}