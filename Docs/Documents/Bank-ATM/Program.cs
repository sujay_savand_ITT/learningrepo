﻿using System;

namespace ATM_machine {
    class ATM {
        static void Main (string[] args) {
            ATMCard atmCard = new ATMCard ();
            ValidatePin validatePin = new ValidatePin ();
            double amount = 0.0;
            double withdrawalAmount = 0.0;
            bool isConnectedToNetwork = true;
            try {
                if (isConnectedToNetwork) {
                    Console.WriteLine ("Welcome");
                    bool isAuthenticated = validatePin.authenticate ();
                    if (isAuthenticated) {
                        Console.WriteLine ("Select Option: ");
                        Console.WriteLine ("\n 1) Withdraw \n2) Balance Enquiry \n 3) Exit");
                        int choice;
                        do {
                            Console.WriteLine ("\nEnter Choice: ");
                            choice = int.Parse (Console.ReadLine ());

                            switch (choice) {
                                case 1:
                                    Console.WriteLine ("Enter amount to withdraw: ");
                                    amount = double.Parse (Console.ReadLine ());
                                    try {
                                        withdrawalAmount = atmCard.withdraw (amount);;
                                    } catch (InsufficientBalanceException e) {
                                        Console.WriteLine ("Insufficient balance in account");
                                    }
                                    Console.WriteLine ("Collect " + withdrawalAmount + " cash");
                                    break;

                                case 2:
                                    Console.WriteLine ("Current Balance: " + atmCard.balanceEnquiry ());
                                    break;

                                case 3:
                                    break;

                                default:
                                    Console.WriteLine ("Please Enter Valid Choice !!");
                                    break;
                            }
                        }
                        while (choice != 3);
                    }
                } else {
                    throw new NetworkException("http://localhost",8080);
                }
            } catch (NetworkException) {
                Console.WriteLine ("Can't connect to server now");
            }
        }
    }
}