//importing Webdriver Class 
//Creates a new WebDriver client, which provides control over a browser
import { WebDriver } from "selenium-webdriver";
let driver: WebDriver;

//importing HomePage Class
let homePage = require('./HomePage');

//importing ComparePage Class
let compareFunction = require('./ComparePage');

//importing ProductFinderPage Class
let productFinder = require('./ProductFinderCategory');
 

//For Selecting the Compare Header
driver = homePage.CompareHeader();

//For Comparing three mobile products
compareFunction.CompareMobiles(driver);

//For Selecting the Product Finder Header
driver = homePage.ProductFinderHeader();

//For Filter Operation (Price range filter)
productFinder.PriceRangeFilter(driver);