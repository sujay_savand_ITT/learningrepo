import { Builder, By,until, Key, WebDriver} from "selenium-webdriver";
let driver : WebDriver;
let compare:any;
let productFinder:any;

//HomePage Class
export class HomePage
{
    //Constructor of this class
    constructor()
    {
        driver = new Builder().forBrowser('chrome').build();
        driver.get('https://gadgets.ndtv.com/');
        driver.manage().window().maximize();    
    }
    
    //For selecting the product finder header Category
    ProductFinderHeader(): WebDriver
    {
        driver.wait(until.elementLocated(By.xpath("//a[text()='Product Finder']")));
        productFinder = driver.findElement(By.xpath("//a[text()='Product Finder']")).click();    
        return driver;
    }

    //For selecting the Compare Header Category
    CompareHeader(): WebDriver
    {
        driver.wait(until.elementLocated(By.xpath("//a[text()='Compare']")));
        compare =  driver.findElement(By.xpath("//a[text()='Compare']")).click();
        return driver;
    }    
}
module.exports = new HomePage();