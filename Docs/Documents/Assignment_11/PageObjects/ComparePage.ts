import { Builder, By,until, Key, WebDriver} from "selenium-webdriver";
let clickButton: any;
//Compare Class to compare different mobile phones
export class Compare
{
    //Three mobilePhones are added and Compared
    async CompareMobiles(driver: WebDriver)
    {
        //1st mobile is added to compare box
        await driver.wait(until.elementLocated(By.id("productcompare1"))); 
        await driver.findElement(By.id("productcompare1")).sendKeys("oppo f11",Key.RETURN);
        await driver.wait(until.elementLocated(By.xpath("//div[text()=' (128GB)']")));
        await driver.findElement(By.xpath("//div[text()=' (128GB)']")).click(); 

        //2nd mobile is added to compare box
        await driver.wait(until.elementLocated(By.id("productcompare2")));
        await driver.findElement(By.id("productcompare2")).sendKeys("samsung galaxy m21",Key.RETURN);
        await driver.wait(until.elementLocated(By.xpath("//div[text()='Samsung Galaxy M21']")));
        await driver.findElement(By.xpath("//div[text()='Samsung Galaxy M21']")).click(); 

        //3rd mobile is added to compare box
        await driver.wait(until.elementLocated(By.id("productcompare3")));
        await driver.findElement(By.id("productcompare3")).sendKeys("realme 3 pro",Key.RETURN);
        await driver.wait(until.elementLocated(By.xpath("//span[text()='Starts from ₹ 8,999']")));     
        await driver.findElement(By.xpath("//span[text()='Starts from ₹ 8,999']")).click();

        //For clicking the compare button
        await driver.wait(until.elementLocated(By.xpath("//a[@id='compareButton']")));
        clickButton= await driver.findElement(By.xpath("//a[@id='compareButton']")).click();
    }    
}
module.exports = new Compare();
