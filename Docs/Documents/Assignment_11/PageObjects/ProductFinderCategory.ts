import { Builder, By,until, Key, WebDriver} from "selenium-webdriver";
let driver: WebDriver = new Builder().forBrowser('chrome').build();
let productFinder: any;
//Class ProductFinder
export class ProductFinder
{
    //For Price filter Operation on mobiles
    async PriceRangeFilter(driver: WebDriver)
    {   
        await driver.findElement(By.xpath("//input[@id='min_price']")).sendKeys('10000', Key.RETURN);
        await driver.wait(until.elementLocated(By.xpath("//input[@id='min_price']")));
        await driver.findElement(By.xpath("//button[@id='price-range-submit']")).click();
        await driver.wait(until.elementLocated(By.xpath("//button[@id='price-range-submit']")));
        return driver;
    }
}
module.exports = new ProductFinder();