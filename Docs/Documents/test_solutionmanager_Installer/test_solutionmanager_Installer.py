"""
$$$$$_BEGIN_TEST_METADATA_DECLARATION_$$$$$
    +purpose: Basic installer/uninstall operation check.
    +test_tier: 1
    +is_manual:False
    +test_classification: 1
    +reqid: DUNE-17492
    +timeout: 120
    +asset: CDM
    +test_framework: TUF
    +name: test_getSolutionManagerInstaller
    +test:
        +title: test_getSolutionManagerInstaller
        +guid:d54d329f-4b7b-4dc9-8e50-f4ac37cf3e69
        +dut:
            +type:Simulator
$$$$$_END_TEST_METADATA_DECLARATION_$$$$$
"""
def extract_values(obj, key):
    """Pull all values of specified key from nested JSON."""
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    results = extract(obj, arr, key)
    return results
# =============================================================================

def test_postInstallerUninstall(cdm):    
    import re
    import pytest
    import requests
    import json       

    URL = "cdm/solutionManager/v1/installer/uninstall"
    body = {"resourceId": "e3736199-5163-4c47-88d3-572e4e06dd6a"}  
    response = requests.post(URL, data = json.dumps(body))  
    assert response.status_code ==200

    # You can extract json two ways: I used both to drill down
    jsonLoad = json.loads(response.text)
    jsonResponse = response.json()   
    # Validate each dictionary pair in JSON response is identical in both extract methods above
    # Debug Code: step through loops and validate both methods are identical
    i = 1
    for key, value in jsonResponse.items():
        #print(key, ":", value)
        i = i + 1
    # Debug each dictionary pair in JSON load
    i = 1
    for key, value in jsonLoad.items():
        #print(key, ":", value)
        i = i + 1

    opMeta = jsonResponse["opMeta"]
    print(opMeta)
    assert opMeta.get("contentFilter") == ['A dummy content filter']
    
    # for key,value in opMeta
    extractedTypeGUN = extract_values(response.json(), "typeGUN")
    extractedcontentValue = extract_values(response.json(), "value")
    typeGUN = extractedTypeGUN
    value = extractedcontentValue

    assert typeGUN == ['"Example typeGUN"']
    assert value == ['null']
    
    #print(typeGUN)
    #print(value)
    
                    
    # ========================================================================
    # Validate the body of the extractlink in the response
    # adding these tests since will be needed in future.
    # ========================================================================


    extractedHref = extract_values(response.json(), "href")
    extractedRel = extract_values(response.json(), "rel")
    extractedState = extract_values(response.json(), "state")

    # validate link on response body ['href', 'self', 'unavailable']

    assert extractedHref == '/solutionManager/v1/installer/uninstall'
    assert extractedRel == 'self'
    assert extractedState == 'unavailable'

    #################################################################################
    
    extractedResourceId = extract_values(response.json(), "resourceId")
    extractedArchiveType = extract_values(response.json(), "archiveType")
    extractedCompletedTimestamp = extract_values(response.json(), "completedTimestamp")
    extractedCreatedTimestamp = extract_values(response.json(), "createdTimestamp")
    
    assert extractedResourceId == "e3736199-5163-4c47-88d3-572e4e06dd6a"
    assert extractedArchiveType == "atSolutionArchive"
    #assert extractedCompletedTimestamp == "2018-04-25T16:20:43.014Z"
    #assert extractedCreatedTimestamp == "2018-04-25T16:20:42.044Z"

    # =================================================================

    extractedErrorCode = extract_values(response.json(), "errorCode")
    extractedErrorDescription = extract_values(response.json(), "errorDescription")

    assert extractedErrorCode == "0"
    assert extractedErrorDescription == "Additional description of the Error"

    ##########################################################################
    
    extractedDescription = extract_values(response.json(), "description")
    extractedName = extract_values(response.json(), "name")
    extractedVersion = extract_values(response.json(), "version")

    assert extractedDescription == "Description of the modification"
    assert extractedName == "Name of the modification"
    assert extractedVersion == "Version of the modification"

    ######################################################################

    extractedModificationId = extract_values(response.json(), "")



