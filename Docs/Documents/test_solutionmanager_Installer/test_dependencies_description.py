import pytest
import requests

def extract_values(obj, key):
    """Pull all values of specified key from nested JSON."""
    arr = []

    def extract(obj, arr, key):
        """Recursively search for values of key in JSON tree."""
        if isinstance(obj, dict):
            for k, v in obj.items():
                if isinstance(v, (dict, list)):
                    extract(v, arr, key)
                elif k == key:
                    arr.append(v)
        elif isinstance(obj, list):
            for item in obj:
                extract(item, arr, key)
        return arr

    results = extract(obj, arr, key)
    return results
#############################################################################

def test_for_dependencies_(udw):
    
    r = cdm.get_raw(URL)
    jsonLoad = json.loads(r.text)
    jsonResponse = r.json() 

##############################################################################

    dependencies = jsonResponse["dependencies"]
    print(dependencies)

    resourceID = jsonResponse["resourceID"]
    assert resourceID == "e3736199-5163-4c47-88d3-572e4e06dd6a"

    extractedfwRevision = extract_values(r.json(), "fwRevison")
    fwRevision = extractedfwRevision
    assert fwRevision == "*"

    #extractedKey = extract_values(r.json(), "key")

    for item in jsonLoad["platformRequirements"]:
        if item['key'] == 'Accessory':
            assert item['value'] == 'OutputStacker'

    assert dependencies.get("requiredSolutions") == ['23BD59A5-8EE6-4FEF-92C5-FBC5D57BF95F']

    description = jsonResponse["description"]
    assert description == "Sign documents with the boss's signature"

    name = jsonResponse["name"]
    assert name == "ACME Signed by the Boss"

    supportEmail = jsonResponse["supportEmail"]
    assert supportEmail == "help@solutions.acme.com"

    supportPhone = jsonResponse["supportPhone"]
    assert supportPhone == "+9999999999"

    supportUrl = jsonResponse["supportUrl"]
    assert supportUrl == "https://solutions.acme.com/bosssign/help"

    vendor = jsonResponse["vendor"]
    assert vendor == "ACME Inc."

    version = jsonResponse["version"]
    assert version == "1.0.1"

    installationDate = jsonResponse["installationDate"]
    assert installationDate == "2018-04-25T16:20:42.044Z"

    packageName = jsonResponse["packageName"]
    assert packageName == "AcmeSolution.bdl"
    
#############################################################################
    
    opMeta = jsonResponse["opMeta"]
    assert opMeta.get("contentFilter") == ['A dummy content filter'] 