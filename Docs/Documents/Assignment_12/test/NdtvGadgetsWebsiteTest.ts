///<summary> Importing HomePage class</summary>
import {HomePage} from '../pages/Homepage'

///<summary> Importing ProductFinderPage class</summary>
import {ProductFinderPage} from '../pages/ProductFinderPage'

///<summary> Importing ComparePage class</summary>
import { ComparePage } from '../pages/ComparePage';

///<summary>Chai is an assertion library for provides functions and methods that helps to compare the output of a certain test with its expected value</summa>
import {expect} from 'chai';

///<summary> creating instance for HomePage class</summary>
let home = new HomePage();

///<summary> creating instance for ProductFinderPage class</summary>
let products = new ProductFinderPage();

///<summary> creating instance for ComparePage class</summary>
let compareProducts = new ComparePage();

///<summary>Import fs module. Node.js includes fs module & is responsible for all the asynchronous or synchronous file I/O operations.</summary>
const fs = require('fs');
///<summary> read the file into raw data</summary>
let rawdata = fs.readFileSync('C:\\Users\\sujay.v\\Documents\\Assignment-12\\productsData\\productsNames.json')
///<summary> parse the raw data into meaningful JSON format</summary>
var productsData = JSON.parse(rawdata);

///<summary>
///Read the string map and retrieve the values respectively
///</summary>
var product1Data = productsData["product1"];
var product2Data = productsData["product2"];
var product3Data = productsData["product3"];

///<summary>By default WebdriverIO provides an assertion library that is built-in</summary>
describe('NDTV Gadgets 360 website Test', function()
{
    it('Open NDTV Gadgets 360 website', function()
    {
        home.openUrl;
    });

    it('Maximize the window', function()
    {
        home.maximizeWindowFunction;
    });

    it('Verifying Title of the Page', function()
    {
        expect('Tech News, Latest Technology, Mobiles, Laptops – NDTV Gadgets 360').to.equal(home.pageHeader);
    });

    it('Product Finder click functionality', function()
    {
        expect("//a[text()='Product Finder']").to.exist;
        home.productFinder.waitForClickable();
        home.productFinder.click();
    });
    
    it('Verifying the title of the Product Finder Page', function()
    {
        expect('Mobile Phones: Latest & New Mobiles, Smartphones Online 2020, Phone Finder').to.equal(products.ProductFinderPageHeader);
    });
    
    it('Select apple brand',function()
    {
        expect("//input[@id='brand_Apple']").to.exist;
        products.selectAppleBrand.waitForClickable();
        products.selectAppleBrand.click();
    });
    
    it('Click Compare Products category', function()
    {
        expect("//a[text()='Compare']").to.exist;
        products.compareProducts.waitForClickable();
        products.compareProducts.click();
    });
    
    it('Verifying the title of the Compare Header Page', function()
    {
        expect('Compare Mobile, Compare Mobile Phones Prices, Compare Smartphones').to.equal(compareProducts.ComparePageHeader);
    });
    
    it('1st Product is added to compare box',function()
    {
        compareProducts.product1.waitForExist();
        expect("//input[@id='productcompare1']").to.exist;
        compareProducts.product1.waitForExist();
        compareProducts.product1.setValue(product1Data);
    });
    
    it('1st Product added is selected',function()
    {
        compareProducts.product1Select.waitForExist();
        expect("//div[text()='Samsung Galaxy M21']").to.exist;
        compareProducts.product1Select.waitForClickable();
        compareProducts.product1Select.click();       
    });
    
    it('2nd Product is added to compare box',function()
    {
        compareProducts.product2.waitForExist();
        expect("//input[@id='productcompare2']").to.exist;
        compareProducts.product2.waitForExist();
        compareProducts.product2.setValue(product2Data);
    });
    
    it('2nd Product added is selected',function()
    {
        compareProducts.product2Select.waitForExist();
        expect("//span[text()='Starts from ₹ 17,899']").to.exist;
        compareProducts.product2Select.waitForClickable();
        compareProducts.product2Select.click();
    });
    
    it('3rd Product is added to compare box',function()
    {
        compareProducts.product3.waitForExist();
        expect("//input[@id='productcompare3']").to.exist;
        compareProducts.product3.waitForExist();
        compareProducts.product3.setValue(product3Data);
    });
    
    it('3rd Product added is selected',function()
    {
        compareProducts.product3Select.waitForExist();
        expect("//div[text()='Redmi Note 9 Pro']").to.exist;
        compareProducts.product3Select.waitForClickable();
        compareProducts.product3Select.click();
    }); 
    
    it('Compare button click functionality',function()
    {
        compareProducts.compareButtonSelect.waitForExist();
        expect("//a[@id='compareButton']").to.exist;
        compareProducts.compareButtonSelect.waitForClickable();
        compareProducts.compareButtonSelect.click();
    }); 
    
    it('Verifying the title of the Compare Products Page', function()
    {
        expect('Compare Samsung Galaxy M21 vs Redmi Note 9 Pro vs Realme 6 Pro Price, Specs, Ratings').to.equal(compareProducts.ComparePageHeader);
    });       
});