///<summary>
///<c>HomePage Class</c>
///</summary>
export class HomePage 
{
    ///<summary>For navigating to the URL(https://gadgets.ndtv.com/)</summary>
    get openUrl() 
    {
        return browser.url('/');
    }
    
    ///<summary>For maximizing the window</summary>
    get maximizeWindowFunction()
    {
        return browser.maximizeWindow;
    }

    ///<summary>For verifying Title of the Page</summary>
    get pageHeader()
    {
        return browser.getTitle();          
    }

    ///<summary>For selecting the product finder header category</summary>
    get productFinder()
    {
        return $("//a[text()='Product Finder']");
    }
}
