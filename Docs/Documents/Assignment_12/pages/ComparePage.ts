///<summary>
///<c>ComparePage Class</c>
///</summary>
export class ComparePage
{
    ///<summary>For verifying the title of the Compare Header Page</summary>
    get ComparePageHeader()
    {
        return browser.getTitle();
    }

    ///<summary>For adding 1st Product to compare box</summary>
    get product1()
    {
        return $("//input[@id='productcompare1']");
    }

    ///<summary>For selecting 1st product for comparing</summary>
    get product1Select()
    {
        return $("//div[text()='Samsung Galaxy M21']");
    }
   
    ///<summary>For adding 2nd Product to compare box</summary>
    get product2()
    {
        return $("//input[@id='productcompare2']");
    }

    ///<summary>For selecting 2nd product for comparing</summary>
    get product2Select()
    {
        return $("//span[text()='Starts from ₹ 17,899']");
    }

    ///<summary>For adding 3rd Product to compare box</summary>
    get product3()
    {
        return $("//input[@id='productcompare3']");
    }
   
    ///<summary>For selecting 3rd product for comparing</summary>
    get product3Select()
    {
        return $("//div[text()='Redmi Note 9 Pro']");
    }
    ///<summary>For clicking the compare button</summary>
    get compareButtonSelect()
    {
        return $("//a[@id='compareButton']");
    }
}