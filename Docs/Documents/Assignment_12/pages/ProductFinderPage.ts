///<summary>
///<c>ProductFinderPage Class</c>
///</summary>
export class ProductFinderPage
{
    ///<summary>For obtaining the title of the Product Finder Page</summary>
    get ProductFinderPageHeader()
    {
        return browser.getTitle();
    }

    ///<summary>For selecting apple brand</summary>
    get selectAppleBrand()
    {
        return $("//input[@id='brand_Apple']");
    }

    ///<summary>For clicking Compare Products category</summary>
    get compareProducts()
    {
        return $("//a[text()='Compare']");
    }
}