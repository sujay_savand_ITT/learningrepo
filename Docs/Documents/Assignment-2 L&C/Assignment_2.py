def checkUserInputIsInRange(number):

    if number.isdigit() and 1 <= int(number) <= 100:
        return True
    else:
        return False


def CompareInputAndRandomNumber(inputNumber, randomNumber):

    if inputNumber < randomNumber:
        inputNumber = input("Too low. Guess again")

    else:
    inputNumber = input("Too High. Guess again")

    return inputNumber


def main():

    randomNumber = random.randint(1, 100)
    condition = False
    userInputNumber = input("Guess a number between 1 and 100:")
    chancesToGuessCorrectNumber = 0


while not condition:

    if not checkUserInputIsInRange(inputNumber):
        inputNumber = input(
            "I wont count this one Please enter a number between 1 to 100")
        continue
    else:
        chancesToGuessCorrectNumber += 1
        inputNumber = int(inputNumber)

inputNumber = CompareInputAndRandomNumber(inputNumber, randomNumber)

if (inputNumber == randomNumber):
    print("You guessed it in", chancesToGuessCorrectNumber, "guesses!")
    condition = True

main()
