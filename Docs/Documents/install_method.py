import json
import requests
import os

oauth2_admin_header = {
    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWJqZWN0IjoiYWRtaW4iLCJzY29wZXMiOiJjb20uaHAuY2RtLmF1dGguYWxpYXMuZGV2aWNlUm9sZS5kZXZpY2VBZG1pbiIsImlhdCI6MTU5NDQwMzgwNSwiZXhwIjozMTg4ODA0Nzk5LCJhdWQiOiJlMnNpbXVsYXRvciIsImlzcyI6ImUyc2ltdWxhdG9yIn0.aX-x8z0yDGrcaqjmXY8v3wesHDTIDu41z27745wp6Y4'}

url = 'http://localhost/cdm/e2/services/solutionManager/v1/installer/install'

def test_solution_install_method():
    zip_file = os.path.basename(
        "src/test/tests/extensibility/solution_manager/usb_registration_bundle.zip")

    payload = { "archiveType": "atSolutionArchive"}
    payload_data = {"data": [
        {
                "key": "$MACRO1$",
                "value": "Value1"
                },
        {
            "key": "$MACRO2$",
            "value": "Value2"
        }
    ]}
    files = {
        'content': (None, json.dumps(payload), 'application/json'),
        'context': (None, json.dumps(payload_data), 'application/json'),
        'solution': (zip_file, open(zip_file, 'rb'), 'application/vnd.hp.solution-bundle')
    }
    response1 = requests.post(url, files=files, headers=oauth2_admin_header)
    assert response1.status_code == 200
