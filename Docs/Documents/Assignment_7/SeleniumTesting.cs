using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace Assignment_7
{
    ///<summary> Test class to add the products to the cart and verifying it </summary> 
    public class SeleniumTesting
    {
        ///<summary> A driver object is declared</summary>
        IWebDriver driver;
        int count=0;
        WebDriverWait wait;
        string searchTab ="field-keywords";
        string addToCartButton ="add-to-cart-button";
        string searchNavigator = "nav-input";
        string product1Xpath = "//span[text()='Apple iPhone XR (64GB) - Black']";
        string product2Xpath = "//span[text()='HP 14-inch Laptop (9th Gen A4-9125/4GB/1TB HDD/Win 10/MS Office 2019/AMD Radeon R3 Graphics), 14-cm0123au']";
        string product3Xpath = "//span[text()='Mi 80 cm (32 inches) 4C PRO HD Ready Android LED TV (Black)']";    

        ///<summary>Method to add the products to the cart</summary>
        ///<param1> productName as string for searching the product </param1>
        ///<param2> Each product's Xpath is passed as a parameter</param2>
        ///<return> count is returned after each product is added to cart</return>
        public int AddProductstoCart(string productName, string productXpath)
        {
            IWebElement searchProduct = driver.FindElement(By.Name(searchTab));
            searchProduct.SendKeys(productName);
            IWebElement search_navigator = driver.FindElement(By.ClassName(searchNavigator));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(search_navigator));
            search_navigator.Click();

            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(productXpath)));
            driver.FindElement(By.XPath(productXpath)).Click();
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.Id(addToCartButton)));
            driver.FindElement(By.Id(addToCartButton)).Click();
            count++;
            return count;
        }

        ///<summary>Setup method</summary>
        [SetUp]
         public void Setup()
        {
            ///<summary> A driver object is instantiated</summary>
            driver = new ChromeDriver();
            ///<summary>navigate to URL</summary>
            driver.Navigate().GoToUrl("https://www.amazon.in/");
            ///<summary>Maximize the browser window</summary>
            driver.Manage().Window.Maximize();
        }

        ///<summary>Test method to verify that every products are added to cart </summary>  
        [Test]
        public void AmazonTest()
        {   
            AddProductstoCart("mobiles",product1Xpath);

            AddProductstoCart("hp Laptops",product2Xpath);

            AddProductstoCart("Televisions",product3Xpath); 
              
            int countProducts = count;
            ///<summary>finally viewing the cart by clicking the cart button and verifying its existence in the cart</summary>
            IWebElement final_cart_view = driver.FindElement(By.Id("attach-close_sideSheet-link"));
            wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(final_cart_view));
            final_cart_view.Click();
            Assert.That(3,Is.EqualTo(countProducts));
        }

        ///<summary>TearDown method</summary>
        [TearDown]
        ///<summary>To Close the Browser</summary>
        public void Dispose()
        {
            driver.Close();
        }
    }
}