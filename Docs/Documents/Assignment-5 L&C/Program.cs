﻿// Question: 2. Look at the below classes and the client code given below on how the object are used and methods invoked.Is there a better way to write the Customer class ?

public class Customer {
    private string firstName = "Sujay";
    private string lastName = "Savand";
    private bool billPaid = false;

    public string getFirstName () {
        return firstName;
    }

    public string getLastName () {
        return lastName;
    }

    public bool getPayment (float billAmount) {
        Wallet myWallet = new Wallet ();
        if (myWallet.getTotalMoney () >= billAmount) {
            myWallet.subtractMoney (billAmount);
            billPaid = true;
        }
        return billPaid;
    }
}

public class Wallet {
    private float totalMoney = 100.00f;

    public float getTotalMoney () {
        return totalMoney;
    }

    public void setTotalMoney (float amount) {
        totalMoney = amount;
    }

    public void addMoney (float deposit) {
        totalMoney += deposit;
    }

    public void subtractMoney (float debit) {
        totalMoney -= debit;
    }
}

class DeliveryBoy {
    static void Main (string[] args) {
        float billAmount = 2.00f;
        bool isBillPaid;
        Customer customer = new Customer ();
        isBillPaid = customer.getPayment (billAmount);
        if (isBillPaid) {
            System.Console.WriteLine ("Paid successfully!");
        } else {
            System.Console.WriteLine ("No sufficient amount. Come back later!");
        }
    }
}