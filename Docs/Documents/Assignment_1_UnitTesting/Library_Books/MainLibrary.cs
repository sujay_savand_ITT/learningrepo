using System;
namespace Library
{
    ///<summary>Main BookOperation program class <c>Book</c> <summary>
    class Book                   
        {
            public static void Main(String[] args)
            {
                int bookid,i,select=1;
                string title;
                bool choice=true;
                ///<summary>Object instantiation to BookProgram class</summary>
                BookProgram[] books=new BookProgram[100];         
                Console.WriteLine("Total number of books:");
                ///<summary>Number of books given by the user</summary>
                int n=int.Parse(Console.ReadLine());       
                for(i=0;i<n;i++)
                {
                  ///<summary>calling constructor class</summary>
                  books[i]=new BookProgram();       
                }
                while(choice)                   
                {
                Console.WriteLine("Enter the choice you want:\n1.Add book\n2.Search a book\n3.Update book\n4.Delete or Remove a book\n");
                select = Convert.ToInt32(Console.ReadLine());

                ///<summary>Selecting the operations to be performed</summary>
                switch(select)             
                {       
                    ///<summary> Adding book details </summary>                                           
                    case 1:                                          
                        for(i=0;i<n;i++)
                        {
                            Console.WriteLine("Enter BookID");     
                            bookid=int.Parse(Console.ReadLine());
                            Console.WriteLine("Enter Title");
                            title=Console.ReadLine();
                            ///<summary>Adding Books to books array by calling Add_Book function</summary>
                            books[i].Add_Book(bookid,title);        
                        }
                        for(i=0;i<n;i++)
                        {
                            books[i].Display_Book_Status();
                        }
                        break;

                    ///<summary>for searching a book</summary>
                    case 2:
                        Console.WriteLine("Enter the bookid you want to Search");      
                        bookid=int.Parse(Console.ReadLine());
                        for(i=0;i<n;i++)
                        {
                            bool value=books[i].Search_Book(bookid);
                            if(value==true)
                            {
                                ///<summary>Searching book when value returns true</summary>
                                books[i].Display_Book_Status();                 
                                break;
                                
                            }
                        }
                        if (i == n)
                        {
                            Console.WriteLine("Book not found!");
                        }
                        break;

                    ///<summary>For updating the details of the book</summary>
                    case 3:                                                         
                        Console.WriteLine("enter the bookid to Update");
                        bookid=int.Parse(Console.ReadLine());
                        Console.WriteLine("Enter updated title");
                        string updated_name=Console.ReadLine();
                        for(i=0;i<n;i++)
                        {
                            bool value=books[i].Search_Book(bookid);
                            if (value==true)
                            {
                                ///<summary>Updating the book status when value returns true</summary>
                                books[i].Update_Book_Status(bookid,updated_name);       
                            }
                        }
                        break;

                    ///<summary>For Removing a book</summary>
                    case 4:                                                         
                        Console.WriteLine("Enter the bookid you want to Delete");
                        bookid=int.Parse(Console.ReadLine());
                        for(i=0;i<n;i++)
                        {
                            bool value=books[i].Search_Book(bookid);
                            if(value==true)
                            {
                                ///<summary>For removing the bookid and title of the book</summary>
                                books[i].Delete_Book(bookid);               
                                Console.WriteLine("Book Removed!");
                                break;
                            }
                        }
                        if (i == n)
                            {
                                Console.WriteLine("Book doesn't exist ");
                                break;
                            }
                        break;

                    ///<summary>To come out of the program</summary>
                    case -1:
                        choice=false;
                        break;
                    default: 
                        Console.WriteLine("Enter valid choice!");
                        break; 
                }
            
            }

        }
    }
}