using System;
namespace Library
{
    ///<summary> Implementing Interface <IBook> </summary>
public class BookProgram: IBook        
    {
        ///<summary> Defining the variables title and bookid </summary>
        public string title;
        public int bookid;

        ///<summary> Method to add the details of the book </summary>
        ///<param value= "bookid" and "Title">  as parameters to Add_Book method </param>
        public Boolean Add_Book(int bookid, string title)       
        {
            if(bookid<=0 || title== "")
            {
                return false;
            }
            else
            {
            this.bookid=bookid;
            this.title= title;
            return true;
            }
        }

        ///<summary> Method to search an existing book </summary>
        ///<param value= "bookid"> as a parameter to Search_Book method </param>
        public Boolean Search_Book(int bookid)                    
        {
            if(bookid<=0)
            {
                if(bookid==this.bookid)
                {
                    return true;
                }
                else 
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        } 

        ///<summary> Method to update the details of an existing book </summary>
        ///<param value= "bookid" and "Title">  as parameters to Update_Book_Status method </param>
        public Boolean Update_Book_Status(int bookid,string title)         
        {
            this.bookid=bookid;
            this.title=title;
            this.Display_Book_Status();
            return true;
        }

        ///<summary> Method to delete an existing book </summary>
        ///<param value= "bookid"> as a parameter to Delete_Book method </param>    
        public void Delete_Book(int bookid)                      
        {
            this.bookid=0;
            this.title=null;
        }

        ///<summary>Method to display the book details///</summary>
        public void Display_Book_Status()                               
        {
            Console.WriteLine("BookID={0}\n Title={1}",this.bookid,this.title);
        }

    }
}