using System;
namespace Library
{
    ///<summary> Creating Interface <I> IBook </I> </summary>
    interface IBook         
    {
        ///<summary> Method to add the details of the book </summary>
        ///<param value= "bookid" and "Title">  as parameters to Add_Book method </param>
        Boolean Add_Book(int bookid, string title);   

        ///<summary> Method to search an existing book </summary>
        ///<param value= "bookid"> as a parameter to Search_Book method </param>
        Boolean Search_Book(int bookid);   

        ///<summary> Method to update the details of an existing book </summary>
        ///<param value= "bookid" and "Title">  as parameters to Update_Book_Status method </param>
        void Update_Book_Status(int bookid,string title);   

        ///<summary> Method to delete an existing book </summary>
        ///<param value= "bookid"> as a parameter to Delete_Book method </param>    
        void Delete_Book(int bookid);                    
    }
}
    
