using System;
using NUnit.Framework;
namespace Library.Tests
{
    [TestFixture]
    /// <summary>This class <c>Tests</c> to test the functionality of the various methods defined in the program
    public class Tests
    {
        
        /// <summary>This test method will test if the expected bookid, title matches with the actual bookid and title passed </summary>
        /// <param> bookid and title of the book</param>
        /// Equal if the expected bookid matches with the actual bookid otherwise not equal
        [TestCase(123, "Harry Potter")]
        public void AddBook_CallingBookidAndTitleOfBook_returnsTrue(int bookid, string title)
        {
            BookProgram book = new BookProgram();
            bool result = book.Add_Book(bookid, title);
            Assert.AreEqual(result, true);
        }

 
        [TestCase(-126, "Harry Potter")]
        public void AddBook_CallingBookidAndTitleOfBook_returnFalse_(int bookid, string title)
        {
            BookProgram book = new BookProgram();
            bool result= book.Add_Book(bookid, title);
            Assert.AreEqual(result, false);
        }
        /// <summary>This test method will test if the expected title of the book matches with the actual bookid and title </summary>
        /// <param> bookid and title of the book</param>
        /// Are Equal if the expected title matches with the testcase title otherwise not equal

        [TestCase(100,"")]
        [TestCase(0,"")]
        [Test]
        public void AddBook_InvalidorValidFormat_returnTrue(int bookid, string title) 
        {
            BookProgram book = new BookProgram();
            bool result = book.Add_Book(bookid,title);
            Assert.AreEqual(result, true);
        }
        /// <summary>This test method will test if the expected bookid and title as testcase of the book matches with the actual bookid and title </summary>
        /// <param>the bookid and title of the book</param>
        /// Are Equal if the expected title matches with the testcases bookid and title

        [TestCase(123)]
        public void SearchBook_SeachExistingBookid_returnTrue(int bookid)
        {
            BookProgram book = new BookProgram();
            bool result = book.Search_Book(bookid);
            Assert.IsTrue(result);
        }
        /// <summary>This test method will test if the expected bookid is searched</summary>
        /// <param> bookid and title of the book</param>
        /// is true if the bookid is searched

        [TestCase(1.5)]
        [TestCase(-123)]
        [Test]
        public void SearchBook_SeachExistingBookid_returnFalse(int bookid)
        {
            BookProgram book = new BookProgram();
            bool result = book.Search_Book(bookid);
            Assert.AreEqual(result,false);
        }
        /// <summary>This test method will test if the expected bookid is searched</summary>
        /// <param> bookid of the book to be searched</param>       
        /// is true if the bookid is searched

        [TestCase(22, "The Dragons")]
        [Test]
        public void UpdateBookStatus_BookidandTitleAsArguments_AreEqual(int bookid, string title)
        {
            BookProgram book = new BookProgram();
            bool result=book.Update_Book_Status(bookid,title);
            Assert.equal(result,true);
        }
        /// <summary>This test method will test if the bookid searched is updated with the title</summary>
        /// <param>the bookid and title of the book</param>
        /// is true if the bookid is searched and title is updated
    }
}