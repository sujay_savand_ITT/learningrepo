using System.IO;
using NUnit.Framework;

namespace File_Operations.Tests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void WriterFile_FileNotExists_ReturnFalse()  //Testcase for WriterFile method if file doesn't exists
        {
            string path = @"text12.txt";    // Path for a non-existing text file
            FilesOperation files = new FilesOperation();   //Object Instantiation to FilesOperation class
            var result = files.WriterFile(path);   //Calling method WriterFile
            Assert.AreEqual(false, result);     //Comparing actual and expected result
        }
        [Test]
        public void WriterFile_FileExists_ReturnTrue()    //Testcase for WriterFile for existing file
        {
            string path = @"text.txt";      // Path for a existing text file
            FilesOperation files = new FilesOperation();    //Object Instantiation to FilesOperation class
            var result = files.WriterFile(path);      //Calling method WriterFile
            Assert.AreEqual(true, result);      //Comparing actual and expected result
        }
         [Test]
        public void ReaderText_FileExists_ReturnTrue()  //Testcase for ReaderText method for existing file
        {
            string path = @"text.txt";        // Path for a existing text file
            FilesOperation files = new FilesOperation();    //Object Instantiation to FilesOperation class
            var result = files.ReaderText(path);      //Calling method ReaderText
            Assert.AreEqual(true, result);      //Comparing actual and expected result
        }
         [Test]
        public void ReaderText_FileExists_ReturnFalse()     //TestCase for ReaderText method if file doesn't exists
        {
            string path = @"test12.txt";        // Path for a non-existing text file
            FilesOperation files = new FilesOperation();    //Object Instantiation to FilesOperation class
            var result = files.ReaderText(path);    //Calling method ReaderText
            Assert.AreEqual(false, result);     //Comparing actual and expected result
        }
        [Test] 
        public void CountLines_GreaterThanZero_ReturnsTrue()
        {
            string path = @"text.txt";          // Path for a existing text file
            FilesOperation files = new FilesOperation();    //Object Instantiation to FilesOperation class
            bool result = files.CountLines(path);    //Calling method CountLines
            Assert.AreEqual(true, result);      //Comparing actual and expected result
        }
        [Test]
        public void ReadLastLine_LastLine_ReturnsLastLine()
        {
            string path = @"text.txt";      // Path for a existing text file
            FilesOperation files = new FilesOperation();    //Object Instantiation to FilesOperation class
            string result = files.ReadLastLine(path);    //Calling method ReadLastLine
            Assert.That(result, Is.Not.Length.EqualTo(0));  //Checks if the last line of file returned or not
        }
    }
}