using System;
namespace File_Operations
{
    class Files    ///File Operations are declared in this class
    {
      public static void Main(string[] args)
      {
          int select= 1;    ///select variable is used for switch operation
          bool choice= true;  ///choice variable is true and will remain in while loop until user enters -1
          Console.WriteLine("Enter the file name:");
          string file_name= Console.ReadLine()+".txt";   ///File name taken from the user 
          string file = @file_name;           
          ///<summary>Defining a path to file_name</summary>
          FilesOperation operation =new FilesOperation(); 
          ///<summary>Object instantiated to FilesOperation Class</summary>
          while(choice)
          {   
          Console.WriteLine("\nEnter the choice:\n1.To create the file\n2.To write into the file\n3.To read the file\n4.To append texts into the file\n5.To count number of lines in a file\n6.To read last line from the file\n7.To delete the file\n");
          select= Convert.ToInt32(Console.ReadLine());    
          switch(select) 
            {       
                case 1:
                  operation.CreateFile(file);        ///<summary> Method to create the file </summary>
                  break;         
                case 2:
                  operation.WriterFile(file);        ///<summary> Method to write into the file </summary>
                  break;
                case 3:
                  operation.ReaderText(file);        ///<summary> Method To read from the file </summary>
                  break;
                case 4:
                  operation.AppendText(file);       ///<summary> Method to append texts to existing file </summary>
                  break;
                case 5:
                  operation.CountLines(file);      ///<summary> Method To count the number of lines in a file </summary>
                  break;
                case 6:
                  operation.ReadLastLine(file);   ///<summary> Method To read the last line of the file </summary>
                  break;
                case 7:
                  operation.DeleteFile(file);      ///<summary> Method To delete the existing file </summary>
                  break;
                case -1:
                  choice=false;           ///comes out of the program when user enters -1 as a choice
                  break;
                default:
                  Console.WriteLine("Enter Valid input choice!");  ///when none of the choice matches, default statement will be executed
                  break;
            }
          }
      }
    }  
}