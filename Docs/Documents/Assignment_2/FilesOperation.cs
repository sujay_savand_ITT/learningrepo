using System;
using System.IO;       //For importing packages for input-output functions
using System.Linq;     //For reading last line of the file
namespace File_Operations
{
class FilesOperation   
{
    ///<summary> Method to create a file, first it will check whether the file is exist or not, if not exist file will be created</summary>
    ///<param= "file">Here file is a string parameter,which is sent to Createfile method</param> 
    public void CreateFile(string file)      
    {
        if(File.Exists(file))
        {
            File.Delete(file);
        }
        else
        {
            Console.WriteLine("Creating the file...\n ");  
        }
        File.Create(file).Dispose();
        Console.WriteLine("File Created! ");  
        WriterFile(file);  
    }

    ///<summary> Method to write into the file</summary>
    ///<param= "file">Here file is a string parameter,which is sent to Writefile method</param> 
    public void WriterFile(string file)      
    {
        if(File.Exists(file))
        {
            using (StreamWriter writer = new StreamWriter(file))    //StreamWriter Class to write to the stream
                {
                    Console.WriteLine("Enter the text to write into the file");
                    string data=Console.ReadLine();     //variable data to write to the stream
                    writer.WriteLine(data);
                }
        }
        else
        {
            Console.WriteLine("File Doesn't exists. Create the file first!");
        }
    }

    ///<summary> Method to read the file</summary>
    ///<param= "file">Here file is a string parameter,which is sent to ReaderText method</param> 
    public void ReaderText(string file)             
    {   
        try                                         //For handling the Exception
        {
            string[] lines = File.ReadAllLines(file);
            foreach (string line in lines)
            {
                Console.WriteLine(line);
            }
        }
        catch(FileNotFoundException)                    
        {
            Console.WriteLine("File not found!"); 
        }
        ///<summary>Initializes a new instance of the FileNotFoundException class with its message string set to a system-supplied message</summary>
    }
    
    ///<summary> Method to append the texts into the file</summary>
    ///<param= "file">Here file is a string parameter,which is sent to AppendText method </param>
    public void AppendText(string file)                 
    {
        if(File.Exists(file))
        {
            using(StreamWriter writer=File.AppendText(file))
            {
                Console.WriteLine("Enter the text for appending into the file");
                string strAppend=Console.ReadLine();
                writer.Write(strAppend+"\n");
                Console.WriteLine("Text appended!...\nPress choice 3 to read the appended text");
            }
        }  
        else
        {
            Console.WriteLine("File doen't exist.");
        } 
    }

    ///<summary> Method to count the number of lines in a file</summary>
    ///<param= "file">Here file is a string parameter,which is sent to CountLines method</param>
    public void CountLines(string file)             
    {
        if(File.Exists(file))
        {
            int lineCount = File.ReadAllLines(file).Length;
            Console.WriteLine("Number of Lines: "+lineCount);
        }
        else
        {
            Console.WriteLine("File Doesn't exist.");
        }
    }

    ///<summary> Method to delete a file</summary>
    ///<param= "file">Here file is a string parameter,which is sent to DeleteFile method</param>
    public void DeleteFile(string file)             //Method To delete the existing file
    {
        try                                        //For Exception handling
        {           
            if(File.Exists(file))
            {
                File.Delete(file);
                Console.WriteLine("File Deleted...");
            }
            else
            {
                Console.WriteLine("File is already deleted");
            }
        }                             
        catch(ArgumentNullException e)                  
        {
            Console.WriteLine("File path is Zero-Length string");
            Console.WriteLine(e.Message);
        }
        /// <summary>To handle the exception if path <c>file_name</c>is null</summary>

        catch(DirectoryNotFoundException e)
        {
            Console.WriteLine("The specified path is invalid");
            Console.WriteLine(e.Message);
        }
        /// <summary>To handle the exception if the specified path<c>file_name</c> is invalid</summary>

        catch(IOException e)                  
        {
            Console.WriteLine("Invalid file name");
            Console.WriteLine(e.Message);         
        }
        /// <summary>To handle the exception if the specified file<c>file</c> is in use</summary>    
    }

    ///<summary> Method to read the last line in the file</summary>
    ///<param= "file">Here file is a string parameter,which is sent to ReadLastLine method</param>
    public void ReadLastLine(string file)           //Method To read the last line of the file
    {
        try
        {
            if(File.Exists(file))
            {
                var lastline= File.ReadLines(file).Last();
                      //This will take to the last statement of the file
                      File.ReadAllLines(file).Last();
                Console.WriteLine(lastline);
            }
            else
            {
                Console.WriteLine("File doesn't exists. Create the file first!");
            }
        }
        catch(ArgumentNullException e)                  
        {
            Console.WriteLine("File path is Zero-Length string");
            Console.WriteLine(e.Message);
        }
        /// <summary>To handle the exception if path <c>file_name</c>is null</summary> 
        
        catch(DirectoryNotFoundException e)
        {
            Console.WriteLine("The specified path is invalid");
            Console.WriteLine(e.Message);
        }
        /// <summary>To handle the exception if the specified path<c>file_name</c> is invalid</summary>

        catch(IOException e)         
        {
            Console.WriteLine(e.Message);
        }   
        /// <summary>To handle the exception if the specified file<c>file</c> is in use</summary>              
    }
}
}