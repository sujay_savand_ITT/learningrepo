// CreateTable function with return-type as void
// rows and cols are the parameters of string type
function CreateTable(rows : string, cols: string): void
{ 
    rows = window.prompt("Input Number of Rows");
    cols = window.prompt("input Number of Column");
    
    for(let i=0; i<parseInt(rows,10); i++)
    {
        let x=document.getElementById('myTable').insertRow(i);
        for(let j=0;j<parseInt(cols,10);j++)  
        {
            let y=  x.insertCell(j);
            y.innerHTML="Row-"+i+" "+" Column-"+j; 
        }
    }
}