//Defining AgeFromDOB(for determining Age from Date of Birth) function
function AgeFromDOB(dateOfBirth : any): void
{
    dateOfBirth = window.prompt("Enter Date Of Birth: Input format(YYYY,MM,DD)");
    const today = new Date();
    const birthDay = new Date(dateOfBirth);
    let age = today.getFullYear()- birthDay.getFullYear();
    const month = today.getMonth()- birthDay.getMonth();
    if( month <0 || (month===0 && today.getDate() < birthDay.getDate()))
    {
        age--;
    }
    //For alerting the age
    alert(age);
}