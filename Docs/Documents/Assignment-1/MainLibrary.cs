using System;
namespace Library
{
    class Book                   //Main BookOperation program class
        {
            public static void Main(String[] args)
            {
                int bookid,i,select=1;
                string title;
                bool choice=true;
                BookProgram[] books=new BookProgram[100];         //Object instantiation to BookProgram class
                Console.WriteLine("Total number of books:");
                int n=int.Parse(Console.ReadLine());        //Number of books given by the user
                for(i=0;i<n;i++)
                {
                  books[i]=new BookProgram();       // calling constructor class
                }
                while(choice)                   
                {
                Console.WriteLine("Enter the choice you want:\n1.Add book\n2.Search a book\n3.Update book\n4.Delete or Remove a book\n");
                select = Convert.ToInt32(Console.ReadLine());
                switch(select)             
                {                                                      //Selecting the operations to be performed
                    case 1:                                          //For Adding a book
                        for(i=0;i<n;i++)
                        {
                            Console.WriteLine("Enter BookID");     
                            bookid=int.Parse(Console.ReadLine());
                            Console.WriteLine("Enter Title");
                            title=Console.ReadLine();
                            books[i].Add_Book(bookid,title);        //Adding Books to books array by calling Add_Book function
                        }
                        for(i=0;i<n;i++)
                        {
                            books[i].Display_Book_Status();
                        }
                        break;
                    case 2:
                        Console.WriteLine("Enter the bookid you want to Search");       //for searching a book
                        bookid=int.Parse(Console.ReadLine());
                        for(i=0;i<n;i++)
                        {
                            bool value=books[i].Search_Book(bookid);
                            if(value==true)
                            {
                                books[i].Display_Book_Status();                 //Searching book when value returns true
                                break;
                                
                            }
                        }
                        if (i == n)
                        {
                            Console.WriteLine("Book not found!");
                        }
                        break;
                    case 3:                                                         //For updating the details of the book
                        Console.WriteLine("enter the bookid to Update");
                        bookid=int.Parse(Console.ReadLine());
                        Console.WriteLine("Enter updated title");
                        string updated_name=Console.ReadLine();
                        for(i=0;i<n;i++)
                        {
                            bool value=books[i].Search_Book(bookid);
                            if (value==true)
                            {
                                books[i].Update_Book_Status(bookid,updated_name);       //Updating the book status when value returns true
                            }
                        }
                        break;
                    case 4:                                                         //For Removing a book
                        Console.WriteLine("Enter the bookid you want to Delete");
                        bookid=int.Parse(Console.ReadLine());
                        for(i=0;i<n;i++)
                        {
                            bool value=books[i].Search_Book(bookid);
                            if(value==true)
                            {
                                books[i].Delete_Book(bookid);               //For removing the bookid and title of the book
                                Console.WriteLine("Book Removed!");
                                break;
                            }
                        }
                        if (i == n)
                            {
                                Console.WriteLine("Book doesn't exist ");
                                break;
                            }
                        break;
                    case -1:
                        choice=false;
                        break;
                    default: 
                        Console.WriteLine("Enter valid choice!");
                        break; 
                }
            
            }

        }
    }
}