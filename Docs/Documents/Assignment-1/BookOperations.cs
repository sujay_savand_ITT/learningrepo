using System;
namespace Library
{
class BookProgram: IBook        //Implenting Interface
    {
        public string title;
        public int bookid;

        public void Add_Book(int bookid, string title)       //Method to add details of the books
        {
            this.bookid=bookid;
            this.title= title;
        }
        public Boolean Search_Book(int bookid)                    //Method to search the book
        {
            if(bookid==this.bookid)
            return true;
            else
            return false;
        } 
        public void Update_Book_Status(int bookid,string title)         //Method to update the details of the books
        {
            this.bookid=bookid;
            this.title=title;
            this.Display_Book_Status();
        }
        public void Delete_Book(int bookid)                      //Method to remove a book
        {
            this.bookid=0;
            this.title=null;
        }
        public void Display_Book_Status()                               //Method to diplay the details of the book
        {
            Console.WriteLine("BookID={0}\n Title={1}",this.bookid,this.title);
        }

    }
}