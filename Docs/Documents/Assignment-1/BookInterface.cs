using System;
namespace Library
{
    interface IBook         //Creating Book Interface
    {
        void Add_Book(int bookid, string title);         //Method to add details of the books
        Boolean Search_Book(int bookid);                     //Method to search the book 
        void Update_Book_Status(int bookid,string title);       //Method to update the details of the books
        void Delete_Book(int bookid);                    //Method to remove a book
    }
}
    
