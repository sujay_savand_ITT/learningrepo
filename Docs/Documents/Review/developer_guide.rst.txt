.. _developer-guide:
Developer Guide
===============

While a Docker based runner is provided, a better development experience can be
obtained if setting a proper Python environment. You will gain stuff like:

* Faster run times
* Integration with code editors (auto-completion, linting, code formatting...)
* Visual debugging on browser based tests

Python
------

You will need Python >= 3.6 installed on your system. You can check your Python
version by running::

        python3 --version

Some systems such as Ubuntu 16.04 come with older Python versions. If that is
your case, you will need to install a newer Python version. It is recommended to
use `pyenv <https://github.com/pyenv/pyenv-installer>`_ for such purpose. It
allows to install multiple Python version on a system and does not require
super-user permissions. In order to install ``pyenv`` simply do::

        curl https://pyenv.run | bash

once done, add these lines to ``~/.bashrc`` (or your shell's rc file)::

        export PATH="$HOME/.pyenv/bin:$PATH"
        eval "$(pyenv init -)"
        eval "$(pyenv virtualenv-init -)"

restart your shell. You can install any Python version like this::

        pyenv install 3.7.0

You can select a specific Python version for the current shell by running::

        pyenv shell 3.7.0
        # now 'python' will point to the selected Python version!
        python --version
        Python 3.7.0

.. note::
        In all guides ``python`` command will be used, assuming it points to
        the correct Python version.

Virtual environment
-------------------

In order to isolate and keep the development environment contained, it is
recommended to create a virtual environment. You can create one like this::

        python -m venv .venv

Every time you start developing you will need to activate the environment by
running::

        source .venv/bin/activate

If you want to exit the virtual environment simply type ``deactivate``.
Some editors such as Visual Studio Code will automatically use the active
environment.

.. note::
        An environment is tied to the Python version used on its creation, so
        once created, you can forget about versions, simply source it and you
        will be ready to go!

Dependencies
------------

Before doing anything, you will need to install dependencies. You can do it
like this::

        pip install -e .

The command above will actually install ``dunetuf`` in development mode
together with its dependencies. As a developer, you may also want to install
development tools (e.g. linter, type-checker...)::

        pip install -r requirements-dev.txt

Building documentation
----------------------

Unfortunately documentation is not yet published, so chances are that you are
reading this document directly from the GitHub repository. In order to enjoy
the documentation you can build it like this::

    python setup.py build_docs

and then open ``build/sphinx/html/index.html``.

Selenium drivers
----------------

Some tests use Selenium in order to run e2e web-based tests. Selenium requires
to have a compatible driver installed. For example, on Ubuntu you can install
Chromium and its adapter like this::

        sudo apt install chromium-browser chromium-chromedriver

You can refer `Selenium documentation <https://www.seleniumhq.org/download/>`_ for
more information.

Coding style
------------

Code should follow `PEP8 <https://pep8.org/>`_ with the exceptions present in
``.flake8`` on the repository root. You can check for errors by running::

    flake8 dunetuf
    flake8 tests

You can also use the ``black`` auto-formatter in order to comply with the
rules and forget about styling problems. Editors such as Visual Studio Code
integrate well with it.

Type checking
-------------

Framework code should have type hints wherever possible. You can check for
type errors by running::

    mypy -p dunetuf

Documentation
-------------

Documentation is built using `Sphinx <https://www.sphinx-doc.org>`_ and is
found under the ``docs`` folder. ``dunetuf`` and tests documentation is
automatically generated from source code, so **make sure your code is properly
documented**. Documentation should follow `Google Styleguide
<https://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings>`_.

You can build the documentation like this::

    python setup.py build_docs
