using System.Linq;
using NUnit.Framework;
using System;
using RestSharp;
using Assignment_6.models;
using RestSharp.Serialization.Json;

namespace Assignment_6
{
    ///<summary> Test Class <c>RestSharpApiTesting</c> </summary>
    [TestFixture]
    class RestSharpApiTesting
    {
        ///<summary> Test method for checking expected statuscode when GET request is made
        ///<param value= "200">as StatusCode for successful GET request
        [TestCase(200)]
        public void GetMethod_ValidURL(int expectedStatusCode)
        {
            var client = new RestClient("https://reqres.in");
            var request = new RestRequest("/api/users/{userid}", Method.GET);
            request.AddUrlSegment("userid",1);
            var response = client.Execute(request);
            Assert.That(expectedStatusCode,Is.EqualTo((int)response.StatusCode),"Invalid url");   
        }

        ///<summary> Test Method for checking the created id,email, firstName and lastName upon successful POST request</summary>
        [TestCase(123, "suj@gmail.com","suj","savand")]
        [TestCase(1.5,"","","")]
        public void PostMethod_check_for_body(int expected_id, string expected_email, string expected_first , string expected_last)
        {
            var client = new RestClient("https://reqres.in");
            var request = new RestRequest("/api/users", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new {id= expected_id, email= expected_email, first_name= expected_first, last_name= expected_last, avatar = "https://s3.amazonaws.com/twitter/sujsavand/123.jpg"});
            var response = client.Execute<Data>(request);
            Assert.That(response.Data.id, Is.EqualTo(expected_id),"Id fetched is not same as expected ");
            Assert.That(response.Data.email, Is.EqualTo(expected_email),"email fetched is not same as expected"); 
            Assert.That(response.Data.firstName, Is.EqualTo(expected_first),"Firstname fetched is not same as expected");
            Assert.That(response.Data.lastName, Is.EqualTo(expected_last),"Lastname fetched is not same as expected");
            Assert.That(response.StatusCode, Is.EqualTo(201),"Response status code is not same as expected status code");          
        }

        ///<summary> Test method for checking expected statuscode over POST request
        ///<param value= "201">as StatusCode for successful POST request
        [TestCase(201)]
        public void PostMethod_check_StatusCode(int expectedStatusCode)
        {
            var client = new RestClient("https://reqres.in");
            var request = new RestRequest("/api/users", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new {id= "123", email= "suj@gmail.com", first_name= "suj", last_name= "savand", avatar = "https://s3.amazonaws.com/twitter/sujsavand/123.jpg"});
            var response = client.Execute<Data>(request);
            Assert.That(expectedStatusCode, Is.EqualTo((int)response.StatusCode),"Response status code is not same as expected status code");
        }

        ///<summary> Test Method for checking the content-type is "application/json" </summary>
        [Test]
        public void PostMethod_check_ContentType()
        {
            var client = new RestClient("https://reqres.in");
            var request = new RestRequest("/api/users", Method.POST);
             request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new {id= "123", email= "suj@gmail.com", first_name= "suj", last_name= "savand", avatar = "https://s3.amazonaws.com/twitter/sujsavand/123.jpg"});
            var response = client.Execute<Data>(request).ContentType;
            Assert.That("application/json; charset=utf-8",Is.EqualTo(response), "Accept-type is not same as the Content-type");
        }

        ///<summary> Test method for checking expected statuscode upon PUT request
        ///<param value= "200">as StatusCode for successful PUT request
        [TestCase(200)]
        public void PutMethod_check_for_updatedStatusCode(int expectedStatusCode)
        {
            var client = new RestClient("https://reqres.in");
            var request = new RestRequest("/api/users", Method.PUT);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new {id= "100", email= "suhas@gmail.com", first_name= "suhas", last_name= "sharma", avatar = "https://s3.amazonaws.com/twitter/suhassharma/100.jpg"});
            var response = client.Execute<Data>(request);
            Assert.That(expectedStatusCode, Is.EqualTo((int)response.StatusCode),"Response status code is not same as expected status code");
        }


        ///<summary> Test Method for checking the updated email upon successful PUT request</summary>
        [TestCase(100,"suhas@gmail.com","suhas","sharma")]
        [TestCase(0,"","","")]
        public void PutMethod_check_Body(int updated_id, string updated_email, string updated_first, string updated_last )
        {
            var client = new RestClient("https://reqres.in");
            var request = new RestRequest("/api/users", Method.PUT);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new {id= updated_id, email= updated_email, first_name= updated_first, last_name= updated_last, avatar = "https://s3.amazonaws.com/twitter/suhassharma/100.jpg"});
            var response = client.Execute<Data>(request);
            Assert.That(response.Data.id, Is.EqualTo((int)updated_id),"Id is not updated over PUT request");
            Assert.That(response.Data.email, Is.EqualTo((string)updated_email),"Email is not updated over PUT request");
            Assert.That(response.Data.firstName, Is.EqualTo((string)updated_first),"FirstName is not updated over PUT request");
            Assert.That(response.Data.lastName, Is.EqualTo((string)updated_last),"LastName is not updated over PUT request");
            Assert.That(response.StatusCode, Is.EqualTo(200),"Response status code is not same as expected status code");

        }

        ///<summary>this test method to delete the resource on the server, is true when statuscode= 204(No Content)</summary>
        ///<param value="204"> as a parameter</param>
        [TestCase(204)]
        public void DeleteMethod_check_statusCode(int expectedStatusCode)
        {
            var client = new RestClient("https://reqres.in");
            var request = new RestRequest("/api/users/{userid}", Method.DELETE);
            request.AddUrlSegment("userid",1);
            var response = client.Execute(request);
            Assert.That(expectedStatusCode,Is.EqualTo((int)response.StatusCode),"Resource on the server is not deleted");
            var client1 = new RestClient("https://reqres.in");
            var request1 = new RestRequest("/api/users/{userid}", Method.GET);
            request.AddUrlSegment("userid",1);
            var response1 = client.Execute(request);
            Assert.AreEqual((int)response1.StatusCode,expectedStatusCode);
        }        
    }
}