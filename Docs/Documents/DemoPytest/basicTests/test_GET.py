import requests
import json
import pytest

def test_get_locations_for_US_90210_check_status_code_equals_200():
     response = requests.get("http://api.zippopotam.us/us/90210")
     assert response.status_code == 200

		
def test_get_locations_for_India_110001_check_status_code_equals_200():
     response = requests.get("http://api.zippopotam.us/IN/110001")
     assert response.status_code == 200

def test_get_locations_for_US_90210_check_content_type_equals_json():
    response = requests.get("http://api.zippopotam.us/us/90210")
    assert response.headers['Content-Type'] == "application/json"
    response_json = json.loads(response.text)
    print(response_json)

def test_get_locations_for_us_90210_check_country_equals_united_states():
     response = requests.get("http://api.zippopotam.us/us/90210")
     response_body = response.json()
     assert response_body["country"] == "United States"

def test_get_locations_for_India_110001_check_country_equals_India():
     response = requests.get("http://api.zippopotam.us/IN/110001")
     response_body = response.json()
     print(response_body)
     assert response_body["country"] == "India"