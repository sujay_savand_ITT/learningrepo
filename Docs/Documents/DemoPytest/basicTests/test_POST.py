import requests
import json
import pytest

def test_post_body_json():
    url = 'https://reqres.in/api/users'

    headers= {'Content-Type': 'application/json'}

    body = {"id": 1, "email" : "suj888@gmail.com", "first_name" : "Sujay", "last_name" : "Savand"}

    response = requests.post(url, headers = headers, data = json.dumps(body))
    response_body = response.json()

    assert response.status_code == 201
    assert response_body["id"] == 1
    assert response_body["email"] == "suj888@gmail.com"
    assert response_body["first_name"] == "Sujay"
    assert response_body["last_name"] == "Savand"

    #tests to fail
    #assert response.status_code == 200
    #assert response_body["first_name"] == "Suj"