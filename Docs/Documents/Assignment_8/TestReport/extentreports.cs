using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;

namespace SeleniumTesting2
{
    ///<summary>Extent Report Class</summary>
    public class ExtentReport
  {
      public static ExtentReports Instance()
      {
          var htmlReporter = new ExtentHtmlReporter("C:\\Users\\sujay.v\\Documents\\Assignment-8\\TestReport\\ExtentReport.html");
          var extent = new ExtentReports();
          extent.AttachReporter(htmlReporter);
          extent.AddSystemInfo("HostName:","Sujay.V");
          extent.AddSystemInfo("OS","Windows");
          extent.AddSystemInfo("Browser:","GoogleChrome");
          return extent;
      }
  }
}