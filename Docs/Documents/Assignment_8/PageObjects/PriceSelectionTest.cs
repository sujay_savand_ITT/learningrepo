// using OpenQA.Selenium;
// using OpenQA.Selenium.Chrome;
// using OpenQA.Selenium.Support.UI;

// namespace SeleniumTesting2
// {
//     ///<summary>Class to define the functionality of Selecting the price range of iphone7</summary>
//     public class SelectionBox
//     {
//         ///<summary>Method to define the functionality of Selecting the price range of iphone7</summary>
//         ///<param>IWebDriver <driver>, element1, element2 and element3 as a string type</param>
//         public void Selectionbox(IWebDriver driver, string element1, string element2, string element3)
//         {
//             IWebElement selectionLow = driver.FindElement(By.XPath(element1));
//             selectionLow.SendKeys("10000");
//             IWebElement selectionHigh = driver.FindElement(By.XPath(element2));
//             selectionHigh.SendKeys("30000");
//             IWebElement selectOption = driver.FindElement(By.XPath(element3));
//             selectOption.Click();
//         }
//     }
// }