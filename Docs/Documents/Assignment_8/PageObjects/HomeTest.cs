using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTesting2
{
///<summary>Class to define Search function < Search Navigation Bar > and SearchButton function</summary>
 public class AmazonHomeTest
 {   
          ///<summary>Search function < Search Navigation Bar > </summary>
         ///<param>IWebDriver driver and element as string type</param> 
    public void Search(IWebDriver driver, string element)
    {
        WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
        wait.Until(ExpectedConditions.ElementToBeClickable(By.Id(element)));
        IWebElement searchBox = driver.FindElement(By.Id(element));
        searchBox.SendKeys("iphone 7");
        
    }

        ///<summary>Search function < Search Button > </summary>
        ///<param>IWebDriver driver </param>
        public void SearchButton(IWebDriver driver)
    {
       WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
       wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName("nav-input")));
       IWebElement searchbutton = driver.FindElement(By.ClassName("nav-input"));
       searchbutton.Click();
    }
 }   
}