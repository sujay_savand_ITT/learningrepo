using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTesting2
{
 ///<summary> SelectProductPage Class for defining the functionality of Selecting a product </summary>
 public class SelectProductPageTest
 {
    ///<summary>Method to define the functionality of Selecting iphone7 products</summary>
    ///<param>IWebDriver driver and element as string type</param>
     public int ProductSelect(IWebDriver driver)
     {
        IList<IWebElement> selectProducts = driver.FindElements(By.XPath("//span[@class='a-size-medium a-color-base a-text-normal']"));
        int numberOfProducts = selectProducts.Count;
        int count = 0;
        foreach(IWebElement product in selectProducts)
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath("//span[@class='a-size-medium a-color-base a-text-normal']")));
            product.Click();
            count++;
            ///<summary> Object Instantiation for AddToCart class</summary>
            AddToCartTest cartButton = new AddToCartTest();
            cartButton.AddToCartButton(driver);
        }
        return count;
     }
 }
}