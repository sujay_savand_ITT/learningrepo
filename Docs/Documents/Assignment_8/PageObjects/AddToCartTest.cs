using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTesting2
{
    ///<summary> AddtoCart Class for defining the functionality of AddToCartButton functionality </summary>
 public class AddToCartTest
 {
      ///<summary>AddToCartButton method for adding the product into cart functionality</summary>
     ///<param>IWebDriver driver</param>
     public void AddToCartButton(IWebDriver driver)
     {
         WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
         wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("add-to-cart-button")));
         IWebElement cartbutton = driver.FindElement(By.Id("add-to-cart-button"));
         cartbutton.Click();
     }
 }
}