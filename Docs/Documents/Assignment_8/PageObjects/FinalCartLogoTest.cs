using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTesting2
{
    ///<summary> FinalCartLogo Class for defining the functionality of FinalCartView button </summary>
 public class FinalCartLogoTest
 {
        ///<summary>FinalCartView method for final cart viewing functionality</summary>
        ///<param>IWebDriver driver and element as string type</param>
    public void FinalCartView(IWebDriver driver, string element)
     {
       WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
       wait.Until(ExpectedConditions.ElementToBeClickable(By.XPath(element)));
       IWebElement cartlogo = driver.FindElement(By.XPath(element));
       cartlogo.Click();
     }

        ///<summary>Method for Total Cart Price functionality</summary>
        ///<param>IWebDriver driver and element as string type</param>
    public int TotalCartPrice(IWebDriver driver, string element)
     {
       WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
       wait.Until(ExpectedConditions.ElementToBeClickable(By.Id(element)));
       IList<IWebElement> cartTotalPrice = driver.FindElements(By.Id(element));
       int totalPrice = cartTotalPrice.Count;
       return totalPrice;
     }

      ///<summary>Method for obtaining total products added to cart Price </summary>
      ///<param>IWebDriver driver </param>
    public int FinalTotalProducts(IWebDriver driver)
     {
       WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
       wait.Until(ExpectedConditions.ElementToBeClickable(By.ClassName("sc-list-item-content")));
       IList<IWebElement> cartProducts = driver.FindElements(By.ClassName("sc-list-item-content"));
       int totalProducts = cartProducts.Count;
       return totalProducts;
     }
 }
}