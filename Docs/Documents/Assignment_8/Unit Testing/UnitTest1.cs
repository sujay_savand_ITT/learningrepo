using System;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTesting2.Tests
{
    ///<summary>Test Class for testing the functionality of automating the week-8 assignment testcases using Selenium and NUnit Framework</summary>
    [TestFixture]
    public class SeleniumTest
    {
        ///<summary> <IWebDriver driver> Defines the interface through which the user controls the browser</summary>
        IWebDriver driver;

        ///<summary> Object Instantiation for AmazonHome class</summary>
        AmazonHomeTest home =new AmazonHomeTest();

        ///<summary> Object Instantiation for SelectionBox class</summary>
        //SelectionBox selection = new SelectionBox();

        ///<summary> Object Instantiation for SelectProductpage class</summary>
        SelectProductPageTest product = new SelectProductPageTest();

        ///<summary> Object Instantiation for FinalCartLogo class</summary>
        FinalCartLogoTest cartlogo = new FinalCartLogoTest();

        ///<summary> ExtentReport class reference variable declaration</summary>
        ExtentReports extent;

        ///<summary> ExtentTest class reference variable declaration</summary>
        ExtentTest test;
        
        ///<summary>
        ///SetUp Method
        ///</summary>
        [SetUp]
        public void Init()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://www.amazon.in/");
            driver.Manage().Window.Maximize();
            ///<summary> Object Instantiation for ExtentReports class</summary>
            extent = ExtentReport.Instance();
            ///<summary> Object Instantiation for ExtentTest class</summary>
            test = extent.CreateTest("ExtentTestCase");
        }

        ///<summary>Main Test Method<SeleniumTesting> </summary>
        [Test]
        public void SeleniumTesting()
        {

            try
            {
                home.Search(driver, "twotabsearchtextbox");
                home.SearchButton(driver);
 
                //selection.Selectionbox(driver,"//input[@id='low-price']","//input[@id='high-price']","//input[@value='Go']");
                int count = product.ProductSelect(driver);
             
                cartlogo.FinalCartView(driver, "//span[@id='nav-cart-count']");
                int totalAmount = cartlogo.TotalCartPrice(driver, "sc-subtotal-label-activecart");

                int finalProducts=cartlogo.FinalTotalProducts(driver);
            
                Assert.AreEqual(count, finalProducts);
            }
            catch (Exception) ///<summary>failure screenshot </summary>
            {
                var screenshot = driver.TakeScreenshot();
                var filePath = "C:\\Users\\sujay.v\\Documents\\Assignment-8\\Screenshot\\FailureScreenshot.png";
                string timestamp = DateTime.Now.ToString("yyyy-MM-dd-hhmm-ss");
                ///<summary>save together with the screenshot</summary>
                screenshot.SaveAsFile(filePath + timestamp+".png", ScreenshotImageFormat.Png);
            }                   
        }
        ///<summary>
        ///TearDown Method
        ///</summary>
        [TearDown]
        public void Dispose()
        { 
            extent.Flush();
            driver.Close();
        }
    }
}