import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.InputMismatchException;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Blogs {
	public String createUrlFromInput() {
		try {
			Scanner scan = new Scanner(System.in);
			System.out.println("Enter blog name: ");
			String blogName = scan.next();
			System.out.println("\n Enter The Range : ");
			String postRange = scan.next("[0-9]*-[0-9]*");
			String[] postRangeInNum = postRange.split("-");
			int startValue = Integer.parseInt(postRangeInNum[0]);
			int endValue = Integer.parseInt(postRangeInNum[1]);
			String url="https://" + blogName + ".tumblr.com/api/read/json?type=photo&num=" + endValue + "&start=" + startValue;
			scan.close();
			return url;
		} 
		catch (IOException exception) {
			exception.printStackTrace();
		} 
		return null;
	}

	public String fetchBlogDetails(String urlString) {
		try{
			String serverResponse = "";
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.connect();
			Scanner scanner = new Scanner(url.openStream());
			while (scanner.hasNext()) {
				serverResponse += scanner.nextLine();
			}
			scanner.close();
			return serverResponse;
		}
		catch (IOException exception) {
			exception.printStackTrace();
		} 
		return null;
	}

	public String convertToJSONFormat(String serverResponse){
		String respVarName = "var tumblr_api_read = ";
		String responseInJSON = "";
		while (serverResponse.startsWith(respVarName)) {
			responseInJSON = serverResponse.substring(respVarName.length());
			responseInJSON = responseInJSON.replace(";", "");
			break;
		}
		return responseInJSON;
	}

	public void displayBlogDetails(String responseInJSON) {
		try {
			JSONObject jsonBlogObject = new JSONObject(responseInJSON);
			JSONObject jsonkeyObject = new JSONObject(jsonBlogObject.getJSONObject("tumblelog").toString());
			System.out.println("Title : " + jsonkeyObject.get("title").toString());
			System.out.println("Description : " + jsonkeyObject.get("description").toString());
			System.out.println("Name : " + jsonkeyObject.get("name").toString());
			System.out.println("Number Of Posts : " + jsonBlogObject.get("posts-total").toString());

			JSONArray jsonArray = new JSONArray(jsonBlogObject.getJSONArray("posts").toString());
			for (int i = 0; i < jsonArray.length(); i++) {

				JSONObject postsObject = new JSONObject(jsonArray.get(i).toString());
				System.out.println((i + 1) + ":" + postsObject.get("photo-url-1280").toString());

			}
		} catch (JSONException jsonException) 
		{
			jsonException.printStackTrace();
		} catch (InputMismatchException exception) {
			System.out.println("please provide valid input range");

		}
	}

	public static void main(String[] args) {
		Blogs blogs = new Blogs();
		String url= blogs.createUrlFromInput();
		String serverResponse = blogs.fetchBlogDetails(url);
		String responseInJSON=blogs.convertToJSONFormat(serverResponse);
		blogs.displayBlogDetails(responseInJSON);
	}
}